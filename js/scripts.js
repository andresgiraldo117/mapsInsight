
const state_departaments = [];
const state_actors = [];
const state_countries = [];
const principal_layer = [];
const markertestArray = [];
const mp_years = [];
let obj_seleted = {};
let levelmap = '';
let geo_dep = null;
let geo_country = null;
let state_coutry_sel = null;
let state_colors = false;
let state_colors_seleted = [];
let state_actor_seleted = null;
let set_menu = [];
let arr_cv = [];
let initialstate = true;
let state_menu_coutry = false;
let state_flags = [];

fetch('../flags/flags_reference.json').then(rep_a => rep_a.json()).then(js=>state_flags=js);

let mymap = L.map("mapid",{ zoomControl: false }).setView([-7.115986, -64.316406], 4);

mymap.touchZoom.disable();
mymap.doubleClickZoom.disable();
mymap.scrollWheelZoom.disable();
mymap.boxZoom.disable();
mymap.keyboard.disable();

let range_colors = document.querySelectorAll('#range-colors-content li > div');
let range_colors_description = document.querySelectorAll('#description-modal .circle-modal-element');
let popupOptions = {
    'maxWidth': '500',
    'className' : 'another-popup', 
    'closeButton': false,
}
let obj_migaDePan = {
    "region":null,
    "actor":null,
    "country":null,
    "departament":null,
    "city":null
}

function viewStates(){
    console.log(state_departaments);
    console.log(state_actors);
    console.log(arr_cv);
    console.log(principal_layer);
    console.log(markertestArray);
    console.log(mp_years);
    console.log(obj_seleted);
    console.log(levelmap);
    console.log(geo_dep);
    console.log(state_coutry_sel);
    console.log(state_colors);
    console.log(state_colors_seleted);
    console.log(geo_country);
    console.log(set_menu);
}
function clear_menu(){
    $('div#content-flags-countries div.flags-countries-body').html('')
}
function array_Unique(value, index, self) { 
    return self.indexOf(value) === index;
}
function change_description(data){
    $('p#content-description').html(data);
}
function change_title(data){
    $('#sidebar-wrapper2 h3 > span').html(data);
}
function change_coutries(name,levelname,color){
    $('div#content-flags-countries div.flags-countries-body').append(`          
        <div class="d-flex flex-row  flags-tab">
            <div class="pr-2 py-0"><span style="color:${color}">${levelname}</span></div>
            <div class="p-0 pais">${name}</div>
        </div>`
    );
}
function activeMenu(status){
    if(!status){
        $('#nav-tab').html('');
        $('[menu_name="criminal-dinamics"] #collapse112One1 ul').html('');
        $('[menu_name="criminal-dinamics"] #collapse112Two2 ul').html('');
        $('[menu_name="national-resilience"] #accordionEx212 ul').html('');


        //$('nav.tabbable').hide();
        //$('#nav-tabContent').hide();
        $('#menu-back').hide();
        //$('#heading1One1').hide();
        //$('#heading1Two2').hide();
        //$('#heading1Three3').hide();
        $('#range-data').hide();

        range_colors_description.forEach((targ) => {
            $(targ).css('background-color',`#fff`);
        })
        range_colors.forEach((targ) => {
            $(targ).css('background-color',`#fff`);
        })

    }else{
        $('nav.tabbable').show();
        $('#nav-tabContent').show();
        $('#menu-back').show();
    }
}
function initialmap(status){
    mymap.eachLayer((layer) => mymap.removeLayer(layer));
    L.tileLayer(
        "https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4M29iazA2Z2gycXA4N2pmbDZmangifQ.-g_vE53SD2WrJ6tFX7QHmA",
        {
            attribution:
                'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            id: "mapbox/dark-v10",
            tileSize: 512,
            zoomOffset: -1,
            minZoom: 2
        }
    ).addTo(mymap);
    status === true ? loandData() : null;
    return true
}
function setColorMap(layer,levelcolor,listcolors,name_muni){
    let color_list = listcolors[`color_${levelcolor}`];

    let level_name = '';
    if(levelcolor == 0){
        level_name = listcolors[`si_en`];
    }else if(levelcolor == 1 || levelcolor == 2){
        level_name = listcolors[`nula_en`];
    }else if(levelcolor == 3 || levelcolor == 4){
        level_name = listcolors[`baja_en`];
    }else if(levelcolor == 5 || levelcolor == 6){
        level_name = listcolors[`moderada_en`];
    }else if(levelcolor == 7 || levelcolor == 8){
        level_name = listcolors[`alta_en`];
    }else if(levelcolor == 9 || levelcolor == 10){
        level_name = listcolors[`critica_en`];
    }else{
        level_name = '';
    }
    let aux_index = 0;
    if(levelmap === "coutry"){
        aux_index = 0
    }else if(levelmap === "dep"){
        aux_index = 1
    }else null

    if(layer !== null){
        change_coutries(layer.feature.properties[`NAME_${aux_index}`],(level_name).substring(0,3),color_list);
        layer.setStyle({fillColor : color_list})
    }else{
        change_coutries(name_muni,(level_name).substring(0,3),color_list);
    }
}
function change_colors(item){

    if(item.length > 1){
        item = item.find(rg => rg.country_name == state_coutry_sel);
    }
    if(item.length === 1){
        item = item[0];
    }
    this.change_description(item.description_en);
    this.change_title(item.actor_name_en);
    //this.change_coutries(item.coutries);
    
    range_colors_description.forEach((targ, index) => {
        $(targ).css('background-color',`${item.indicadores['color_'+index]}`);
    })
    range_colors.forEach((targ, index) => {
        $(targ).css('background-color',`${item.indicadores['color_'+index]}`);
    })

    mymap.eachLayer(function (layer) {
        if(layer.feature !== undefined){
            if(levelmap === "coutry"){
                if(item.country_name == layer.feature.properties.NAME_0){
                    setColorMap(layer,item.country_level,item.indicadores);
                }
            }
            if(levelmap === "dep"){
                if(item.departamentos){
                    if(layer.feature.properties.NAME_1 !== undefined){
                        (item.departamentos).map(it => {
                            if(layer.feature.properties.NAME_1 == it.departamento_ic){
                                //layer.setStyle({fillColor : item.indicadores[`color_${it.nivel_departamento}`]});
                                setColorMap(layer,it.nivel_departamento,item.indicadores);
                            }
                        })
                    }
                }
            }
        }
    });
}
async function load_layer_map(name) {
    mymap.spin(true);
    levelmap = "coutry";
    let arr_dep = [];

    markertestArray.map(pin => mymap.removeLayer(pin));

    
    function style_muni(feature) {
        return {
            fillColor: "#636161",
            fillOpacity : 1,
            weight: 2,
            color: '#333532',
            dashArray: '3'
        };
    }
    function style(feature) {
        arr_dep.push(feature);
        return {
            fillColor: "#717070",
            fillOpacity : 1,
            weight: 2,
            color: '#333532',
            dashArray: '2'
        };
    }

    let resp = await fetch(`https://developermaps.xyz/geo_coutries/GEO_country_${(name.toLowerCase()).replace(' ','_')}.geojson`);
    let data = await resp.json();
    
    geo_country = L.geoJson(data, {
        style: {
            fillColor: '#888685',
            fillOpacity : 1,
            weight: 4,
            color: '#333532',
            dashArray: '1'
        },
    }).addTo(mymap).on("click", async (e) => {

        state_coutry_sel = e.layer.feature.properties.NAME_0;
        obj_migaDePan['country'] = state_coutry_sel;
        change_breadcrumb();

        mymap.spin(true);
        levelmap = "dep";
        try {
            let resp = await fetch(`https://developermaps.xyz/geo_depa/GEO_country_${(name.toLowerCase()).replace(' ','_')}.geojson`);
            let data = await resp.json();
            
            geo_dep = L.geoJson(data, {style: style}).addTo(mymap)
            .on("click", async (event) => {

                obj_migaDePan['departament'] = event.layer.feature.properties.NAME_1;
                change_breadcrumb();


                var bounds = event.layer.getBounds();
                var latLng = bounds.getCenter();
                mymap.spin(true);
                levelmap = "dep";
                    try {
                        let resp = await fetch(`https://developermaps.xyz/geo_muni/GEO_country_${(name.toLowerCase()).replace(' ','_')}.geojson`);
                        let data3 = await resp.json();
                        let arr_33 = [];
                        levelmap = "muni";

                        (data3.features).forEach(feat => {
                            if(feat.properties.NAME_1 === event.layer.feature.properties.NAME_1){
                                arr_33.push(feat);
                            }
                        })
                        mymap.eachLayer(function (layer) {
                            if(layer.feature !== undefined && layer.feature.properties.NAME_0 !== state_coutry_sel){
                                layer.setStyle({fillColor : '#888685'});
                            }
                        });
                        
                        let aux_indicadores = state_colors_seleted.find(it_rt => it_rt.country_name == state_coutry_sel)
                        clear_menu();
                        L.geoJson(arr_33, {style: style_muni}).addTo(mymap);
                        mymap.fitBounds(event.layer.getBounds())

                        var greenIcon = L.icon({
                            iconUrl: 'https://image.flaticon.com/icons/svg/787/787535.svg',
                            iconSize:     [20, 20], // size of the icon
                        });
                        state_departaments.map((depaArray) => {
                            depaArray.map(dep => {
                                if(dep.ciudad !== false){
                                    (dep.ciudad).map((itemcity) =>{

                                        if(aux_indicadores !== undefined){
                                            setColorMap(null,itemcity.nivel_ciudad,aux_indicadores.indicadores,itemcity.nombre_ciudad);
                                        }
                                        var marker = L.marker([itemcity.latitud_ciudad, itemcity.longitud_ciudad], {
                                                icon: greenIcon
                                            })
                                        .bindPopup(`<b>${itemcity.nombre_ciudad}`, popupOptions).addTo(mymap)
                                        .on('mouseover', function(e){
                                            this.openPopup()
                                        }).on('mouseout',function(){
                                            this.closePopup()
                                        });
                                    })
                                }
                            })
                        })
                        
                        
                    } catch (error) {
                        console.error(error);
                    }
                    mymap.spin(false);
            })

            mymap.eachLayer(function (layer) {
                if(layer.feature !== undefined && layer.feature.properties.ENGTYPE_1 === undefined){
                    layer.setStyle({fillColor : '#888685'});
                }
            });

            setTimeout(() => {
                clear_menu();
                state_colors ? change_colors(state_colors_seleted) : null;
            }, 500);

            
            
            var bounds = e.layer.getBounds();
            var latLng = bounds.getCenter();
            mymap.fitBounds(geo_dep.getBounds())
            
        } catch (error) {
            console.error('paila depa');
        }
        mymap.spin(false);
    });

    if(!$.isEmptyObject(obj_seleted) || state_menu_coutry === true){
        mymap.fitBounds(geo_country.getBounds());
    }

    mymap.spin(false);
    return geo_country
}
function onClickLayerMarker({target}) {
    initialstate = false;
    loadActors(target.options.objID);
    obj_seleted = target.options;
    let actor_id = target.options.obj.anos_ic;
    let actor_location = target.options.obj.geo;
    let actor_coutries = target.options.coutriesList;

    obj_migaDePan['region'] = target.options.obj.nombre_en
    change_breadcrumb();


    $('#actor-name span').html('');
    $('#actor-name span').html(target.options.obj.nombre_en);
    /* $('div#content-flags-countries div.flags-countries-title').html(`
    <h5 class="mt-4">${target.options.obj.nombre_en}</h5>`); */
    change_description(target.options.obj.descripción_en);
    $('div#content-flags-countries div.flags-countries-body').html('');  
    
    //change_coutries(actor_coutries);

    actor_id.map((country)=>{
        (country.countries_ic).map((coutry_item)=>{
            let departa = coutry_item.categoria[0].actor_ic[0].departamentos;
            state_departaments.push(departa);
        })
    });


    actor_coutries.map((ie,index) => {
        load_layer_map(ie).then(resp => set_menu.push(resp) );
    })
    //mymap.setView([actor_location.lat, actor_location.long], 6);
    
    
    let idjr = setInterval(() => {
        if(set_menu.length === actor_coutries.length){
            setTimeout(() => {
                activeMenu(true);
                var group = new L.featureGroup(set_menu);
                mymap.fitBounds(group.getBounds());
                mymap.removeLayer(this);
                clearInterval(idjr);
            }, 1000);
        }
    }, 500);
    return
}
function loadActors(ID){
    this.activeMenu(false)
    let mp_years2 = [];
    
    let aux_ic_menu = [];
    state_actors.map(inp_ac => {
        if(ID === inp_ac.ID){
            aux_ic_menu.push(JSON.stringify({
                "idActor":      inp_ac.ID, 
                "name":         inp_ac.actor_name_en, 
                "category_1":   inp_ac.category_1,
                "category_2":   inp_ac.category_2,
                "year":         inp_ac.year
            }));
            mp_years2.push(inp_ac.year)
        }
    });
    load_actors_menu(aux_ic_menu);
    

    let btn_list_actors = document.querySelectorAll('[menu_name] ul li');
    $(btn_list_actors).click((e)=>change_actor(e));
    let years_unique = mp_years2.filter( array_Unique );
    years_unique.map(ye => $('#nav-tab').append(`<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" year_r="${ye}" href="#" role="tab" aria-controls="nav-profile" aria-selected="false">${ye}</a>`))
    // ----------------------------------------------------------------------------------------
    let item_menu_year = document.querySelectorAll('a#nav-profile-tab');
    $(item_menu_year).click((e)=>{
        let val1 = $(e.target).attr('year_r');
        let allitem = document.querySelectorAll('#accordionEx1 li')
        allitem.forEach(item => {
            let val2 = $(item).attr('year_t');
            if( val2 === val1){
                $(item).show();
            }else{
                $(item).hide();
            }
        })
    });

    $($('#nav-tab > a')[0]).trigger('click');
    // ----------------------------------------------------------------------------------------
}
function loandData(){
    mymap.spin(true);

    var misCabeceras = new Headers();
    var miInit = { method: 'GET',
        'headers': misCabeceras,
        'mode': 'cors',
        'Content-Type': 'application/json',
        'cache': 'default' 
    };
    
    fetch('https://backend.developermaps.xyz/data?token=ZWI1YTZiYTA1MmViNmJlYWM3ZDZjMmY3MzcyMjMxMGM=',miInit)
    .then(res => res.json())
    .then(response => {
        mymap.spin(false);
        let aux = [];
        const data_actors = response.data;
        console.log(data_actors);

        aux.push(data_actors);
        let pulsingIcon = L.icon.pulse({ iconSize: [40, 40], color: "red" });
        aux.forEach(actor => {
            const objectArray = Object.entries(actor);
            objectArray.forEach(([key]) => {

                let aux_coutry_hover = [];
                (actor[key].anos_ic).map((country)=>{
                    (country.countries_ic).map((coutry_item)=>{
                        aux_coutry_hover.push(coutry_item.country)
                    })
                });
                let country_unique = aux_coutry_hover.filter( array_Unique );

                (actor[key].anos_ic).map(ye => {
                    mp_years.push(ye.years_ic.name);
                    (ye.countries_ic).map(ac => {
                        (ac.categoria).map(ac_m =>{
                            (ac_m.actor_ic).map(ic => {
                                let cat_ry = (ac_m.categoria_ic_select_en).split(' - ');
                                let obj_ic = {
                                    "ID" : key,
                                    "country_name" : ac.country,
                                    "country_level" : ic.niveles.nivel_pais,
                                    "year": ye.years_ic.name,
                                    "icon": ic.ico_actor,
                                    "category_1" : cat_ry[0],
                                    "category_2" : cat_ry[1],
                                    "actor_name_en" : ic.nombre_en,
                                    "actor_name_es" : ic.nombre_es,
                                    "description_en" : ic.descripcion_en,
                                    "description_es" : ic.descripcion_es,
                                    "indicadores" : ac_m.indicadores,
                                    "coutries" : country_unique,
                                    "departamentos" : ic.departamentos
                                }
                                state_actors.push(obj_ic);
                                state_countries.push(ac.country);
                            })
                        })
                    })
                    let cont_ca = document.querySelector(`#nav-tabContent #content-criminalactors`);
                    //$(cont_ca).append(`<li class="nav-item" year_t="${ye.years_ic.name}"><a class="nav-link active" nameActor="${actor[key].nombre_en}" href="#">${actor[key].nombre_en + " - " + ye.years_ic.name}</a></li>`);
                })
                var marker = L.marker([actor[key].geo.lat, actor[key].geo.long], {
                //var marker = L.marker([14.4134, -89.329834], {
                    icon: pulsingIcon,
                    obj: actor[key],
                    objID: key,
                    coutriesList : country_unique
                }).bindPopup(`<b>${actor[key].nombre_en}</b><br><div <div class="hover-countries" style="color: #969696;">${country_unique.join('<br>')}</div>`, popupOptions).addTo(mymap)
                .on("click", onClickLayerMarker)
                .on('mouseover', function(){
                    this.openPopup()
                }).on('mouseout',function(){
                    this.closePopup()
                });

                markertestArray.push(marker);
                $('#content-regions-menu-regions-countries').append(`
                    <div class="item-content-regions-menu-regions-countries" keyid="${key}">${actor[key].nombre_en}</div>
                `);
            });

            let arr_cv = state_countries.filter( array_Unique );
            $('#content-countries-menu-regions-countries').html('');
            let aux_cv_ar = '';
            arr_cv.map(it_gt => {
                let name_rt = (it_gt.toLowerCase()).replace(' ','_');
                let flag = state_flags.find(it_fl => it_fl.official_name_en == it_gt);
                aux_cv_ar += `<div class="item-countries-regions-menu-regions-countries" name_c="${name_rt}">
                    <img src="https://www.worldometers.info/img/flags/${(flag.FIPS).toLowerCase()}-flag.gif"> ${it_gt}
                </div>`;
            });
            $('#content-countries-menu-regions-countries').html(aux_cv_ar);
            let divContry = document.querySelectorAll('#content-countries-menu-regions-countries div[name_c]');

            $(divContry).click(itr => {
                $('[menu_name="criminal-dinamics"] #collapse112One1 ul').html('');
                $('[menu_name="criminal-dinamics"] #collapse112Two2 ul').html('');
                $('[menu_name="national-resilience"] #accordionEx212 ul').html('');
                clear_menu();
                let naCo = $(itr.target).attr('name_c');
                state_menu_coutry = true;

                let aux_ic_menu_2 = [];
                state_actors.map(inp_ac => {
                    let fgh = ((inp_ac.country_name).toLowerCase()).replace(' ','_');
                    if(fgh == naCo){
                        aux_ic_menu_2.push(JSON.stringify({
                            "idActor":      inp_ac.ID, 
                            "name":         inp_ac.actor_name_en, 
                            "category_1":   inp_ac.category_1,
                            "category_2":   inp_ac.category_2,
                            "year":         inp_ac.year
                        }));
                    }
                });
                
                load_actors_menu(aux_ic_menu_2);
                this.loand_m_country(naCo);
                let btn_list_actors = document.querySelectorAll('[menu_name] ul li');
                $(btn_list_actors).click((e)=>change_actor(e));
            });

            let aux_ic_menu = [];
            state_actors.map(inp_ac => {
                    aux_ic_menu.push(JSON.stringify({
                        "idActor":      inp_ac.ID, 
                        "name":         inp_ac.actor_name_en, 
                        "category_1":   inp_ac.category_1,
                        "category_2":   inp_ac.category_2,
                        "year":         inp_ac.year
                    }));
            });


            load_actors_menu(aux_ic_menu);
            let btn_list_actors = document.querySelectorAll('[menu_name] ul li');
            $(btn_list_actors).click((e)=> change_actor(e));


            let men_btn = document.querySelectorAll('.item-content-regions-menu-regions-countries');
            $(men_btn).click((e)=>{
                show_hide_top_menu_content();
                state_menu_coutry = false;
                let tarid = $(e.target).attr('keyid');
                markertestArray.map(it_region => {
                    if(it_region.options.objID == tarid){
                        let objevent = {
                            "target" : it_region
                        }
                        onClickLayerMarker(objevent);
                    }
                })
            });
        })
                    // load years initial 
                    let years_unique2 = mp_years.filter( array_Unique );
                    years_unique2.map(ye => $('#nav-tab').append(`<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" year_r="${ye}" href="#" role="tab" aria-controls="nav-profile" aria-selected="false">${ye}</a>`))
                    let item_menu_year = document.querySelectorAll('a#nav-profile-tab');
                    $(item_menu_year).click((e)=>{
                        let val1 = $(e.target).attr('year_r');
                        let allitem = document.querySelectorAll('#accordionEx1 li')
                        allitem.forEach(item => {
                            let val2 = $(item).attr('year_t');
                            if( val2 === val1){
                                $(item).show();
                            }else{
                                $(item).hide();
                            }
                        })
                    });
                     $($('#nav-tab > a')[0]).trigger('click');
        
                    // -------------------
        arr_cv = state_countries.filter( array_Unique );
        return
    })
}
function btn_back(){
    this.activeMenu(false)
    mymap.spin(true);
    this.change_title('About this project');
    this.change_description('Descripcion de las regiones investigadas Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod dolor sit amet, consectetur adipiscing elit, sed do eiusmod.');
    //this.change_coutries([]);
    clear_menu();
    $('div#content-flags-countries div.flags-countries-title').html('');

    try {
        mymap.eachLayer(function (layer) {
            cloneL.push(layer);
            mymap.removeLayer(layer);
        });
        mymap.setView([-7.115986, -64.316406], 4);

        L.tileLayer(
            "https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4M29iazA2Z2gycXA4N2pmbDZmangifQ.-g_vE53SD2WrJ6tFX7QHmA",
            {
                attribution:
                    'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                    '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                    'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                id: "mapbox/dark-v10",
                tileSize: 512,
                zoomOffset: -1,
                minZoom: 2
            }
        ).addTo(mymap);
        $('p#content-description').html('Descripcion de las regiones investigadas Lorem ipsum dolor sit amet.');
        markertestArray.map(pin => pin.addTo(mymap));
        mymap.spin(false);
    } catch (error) {
        location.reload();
    }
}
function show_Description(){
    document.getElementById('description-modal').style.display='block';
}
function hide_Description(){
    document.getElementById('description-modal').style.display='none';
}
function show_hide_top_menu_content(){

    if(document.getElementById('body-menu-regions-countries').style.display === 'block'){
        document.getElementById('top-menu-svg').style.fill='#7d8384';
        document.getElementById("title-menu-regions-countries").style.color = '#7d8384';
        document.getElementById("title-menu-regions-countries").innerHTML = "List of Countries and Regions";
        document.getElementById("arrow-top-menu-regions-countries").innerHTML = '<svg width="10px" height="10px" viewBox="0 0 16 16" class="bi bi-chevron-right" fill="#7d8384" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"/></svg>';
        document.getElementById('body-menu-regions-countries').style.display = 'none';
    }else{
        document.getElementById('top-menu-svg').style.fill='#fff';
        document.getElementById("title-menu-regions-countries").style.color = '#fff';
        document.getElementById("title-menu-regions-countries").innerHTML = "Collapse list to see the map";
        document.getElementById("arrow-top-menu-regions-countries").innerHTML = '<svg width="10px" height="10px" viewBox="0 0 16 16" class="bi bi-chevron-down" fill="#fff" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"/></svg>';
        document.getElementById('body-menu-regions-countries').style.display = 'block';
    }

}
function change_breadcrumb(){
    let arr_miga = [];
    if(obj_migaDePan.region !== null){
        arr_miga.push(obj_migaDePan.region)
    }
    if(obj_migaDePan.actor !== null){
        arr_miga.push(obj_migaDePan.actor)
    }
    if(obj_migaDePan.country !== null){
        arr_miga.push(obj_migaDePan.country)
    }
    if(obj_migaDePan.departament !== null){
        arr_miga.push(obj_migaDePan.departament)
    }
    if(obj_migaDePan.city !== null){
        arr_miga.push(obj_migaDePan.city)
    }

    let aux_arr_miga = '';
    arr_miga.map(item => {
        aux_arr_miga += `
            <li class="breadcrumb__group">
                <span class="breadcrumb__point" aria-current="page">${item}</span>
                <span class="breadcrumb__divider" aria-hidden="true">›</span>
            </li>
        `;
    })
    let contentBreadcrumb = document.querySelector('#content-breadcrumb ol');
    $(contentBreadcrumb).html(aux_arr_miga);
}
function loand_m_country(name){
    mymap.eachLayer(function (layer) {
        if(layer.feature !== undefined){
            mymap.removeLayer(layer);
        }
    });
    /* initialmap(false); */
    obj_migaDePan['country'] = name.replace('_',' ');
    change_breadcrumb();
    activeMenu(true);
    show_hide_top_menu_content();
    load_layer_map(name);
}
function change_actor(e){
    document.getElementById("mapid").scrollIntoView({block: "center", behavior: "smooth"});
    e.preventDefault();

    state_actor_seleted = e;
    if(initialstate && state_menu_coutry === false){
        let set_ht = [];
        initialstate = false;
        if($.isEmptyObject(obj_seleted)){
            let results33 = arr_cv.map( async (item_coutry) => {
                let dfee = load_layer_map(item_coutry).then((resp) => {
                    set_ht.push(true);
                    return true
                });
                return dfee;
            }) 
            Promise.all(results33).then(() => {
                activeMenu(true);
                internalchange_actor(e);
            });
        }
    }else{
        internalchange_actor(e);
    }

    function internalchange_actor(event){
        state_colors = true;
        let val_id = $(event.target.parentElement).attr('idactor');
        let val_year = $(event.target.parentElement).attr('year_t');
        let val_categ1 = $(event.target.parentElement).attr('iccategory1');
        let val_name = event.target.innerText;
    
        obj_migaDePan['actor'] = val_name;
        change_breadcrumb();
    
        
        $('#range-data').show();
    
        mymap.eachLayer(function (layer) {
            if(layer.feature !== undefined){
                //geo_dep.resetStyle(layer);
                layer.setStyle({fillColor : '#888685'});
            }
    
        });
    
        state_colors_seleted = [];
        this.clear_menu();
        state_actors.map(item => {
            let sta_cat2 = true;
            if(item.ID === val_id && item.year === val_year && item.category_1 == val_categ1 && item.actor_name_en == val_name){
                state_colors_seleted.push(item);
                this.change_colors(item);
            }
        })
    }
}
function load_actors_menu(data){
    let actor_unique = data.filter( array_Unique );
    actor_unique.map(ic_ac => {
        let aux_ac = JSON.parse(ic_ac);
        if(aux_ac.category_1 == "Dinámicas Criminales" || aux_ac.category_1 == "Criminal Dynamics"){
            $('#heading1One1').show();
            if(aux_ac.category_2 == "Actores Criminales" || aux_ac.category_2 == "Criminal Actors"){
                $('[menu_name="criminal-dinamics"] #collapse112One1 ul')
                .append(`<li class="nav-item" year_t="${aux_ac.year}" idActor="${aux_ac.idActor}" icCategory1="${aux_ac.category_1}" icCategory2="${aux_ac.category_2}">
                <a class="nav-link active" href="#">${aux_ac.name}</a></li>`);
            }
            if(aux_ac.category_2 == "Economías Criminales" || aux_ac.category_2 == "Criminal Economies"){
                $('[menu_name="criminal-dinamics"] #collapse112Two2 ul')
                .append(`<li class="nav-item" year_t="${aux_ac.year}" idActor="${aux_ac.idActor}" icCategory1="${aux_ac.category_1}" icCategory2="${aux_ac.category_2}">
                <a class="nav-link active" href="#">${aux_ac.name}</a></li>`);
            }
        }else if(aux_ac.category_1 === "Resistencia Nacional" || aux_ac.category_1 === "National Resilience"){
            $('#heading1Two2').show();
            let tg = document.querySelectorAll('#accordionEx212');
            $(tg[1]).hide();
            //if(aux_ac.category_2 === "Actores Criminales" || aux_ac.category_2 == "Criminal Actors"){
                $('[menu_name="national-resilience"] #accordionEx212 ul')
                .append(`<li class="nav-item" year_t="${aux_ac.year}" idActor="${aux_ac.idActor}" icCategory1="${aux_ac.category_1}" icCategory2="${aux_ac.category_2}">
                <a class="nav-link active" href="#">${aux_ac.name}</a></li>`);
            //}
            /* if(aux_ac.category_2 === "Economías Criminales" || aux_ac.category_2 == "Criminal Economies"){
                $('[menu_name="national-resilience"] #collapse212Two2 ul')
                .append(`<li class="nav-item" year_t="${aux_ac.year}" idActor="${aux_ac.idActor}" icCategory1="${aux_ac.category_1}" icCategory2="${aux_ac.category_2}"><a class="nav-link active" href="#">${aux_ac.name}</a></li>`);
            } */
        }else null;
    })
}

this.initialmap(true);
this.activeMenu(false)



