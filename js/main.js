$(document).ready(function () {
  $("#menu-toggle").click(function (e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
  });

  $(".item_menu_1_show").on("shown.bs.collapse", function () {
    //console.log($(this).data("to"));
    $($(this).data("to")).addClass("active");
  });

  $(".item_menu_1_show").on("hidden.bs.collapse", function () {
    //console.log("Closed");
    $($(this).data("to")).removeClass("active");
  });

  $(".item_menu_2_show").on("shown.bs.collapse", function () {
    //console.log($(this).data("to"));
    $($(this).data("to")).addClass("active");
  });

  $(".item_menu_2_show").on("hidden.bs.collapse", function () {
    //console.log("Closed");
    $($(this).data("to")).removeClass("active");
  });
});
